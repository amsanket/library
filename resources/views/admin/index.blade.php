@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Book List</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="books">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Author</th>
                                    <th>ISBN</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_body_scripts')
    <script type="text/javascript">
        var bookDataUrl = '{{ action('AdminController@bookData') }}'
        jQuery(document).ready(function () {
            jQuery('#books').DataTable({
                "ajax": {
                    "url": bookDataUrl
                },
                "columns": [
                    {"data": "name"},
                    {"data": "author"},
                    {"data": "isbn"},
                    {"data": "quantity"}

                ],
                "serverSide": true
            });
        });
    </script>
@endsection