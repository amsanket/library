@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="card panel-default">
                    <div class="card-header">Book List</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="books">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>User Name</th>
                                    <th>ISBN</th>
                                    <th>Quantity</th>
                                    <th>Status</th>
                                    <th>Actions</th>

                                </tr>
                                </thead>
                                <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_body_scripts')
    <script type="text/javascript">
        var bookDataUrl = '{{ action('AdminController@pendingData') }}'
        jQuery(document).ready(function () {
           fetchData(); 
        });
        function fetchData() 
        {
            jQuery('#books').DataTable({
                "ajax": {
                    "url": bookDataUrl
                },
                "columns": [
                    {"data": "name"},
                    {"data": "user"},
                    {"data": "isbn"},
                    {"data": "quantity"},
                    {"data": "status"},

                ],
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            if (data == 1) {
                                return '<span class="badge badge-info">Approved</span>';
                            } else if(data == 0) {
                                return '<span class="badge badge-danger">Pending</span>';
                            } else {
                                return '<span class="badge badge-success">Returned</span>';

                            }
                        },
                        "targets": 4
                    },
                    {
                        "targets": 5,
                        "render": function ( data, type, row ) {
                            return '<div class="dropdown"> <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre> Actions <span class="caret"></span> </a> <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#" onclick="approve('+row.id+')">Approve</a><a class="dropdown-item" href="#" onclick="returnBook('+row.id+')">Return</a> </div> </div>'
                        },
                    }
                ],
                "serverSide": true
            });
        }
        function approve(id)
        {
            var r = confirm("Are you sure?");
            if (r == true) {
                console.log(id);
                $.ajax({
                    "url": "/admin/issue/approve/"+id,
                    "method":"GET",
                    success: function(response){
                        if(response.code == 200)
                        {
                            alert('Issue Approved Successfully!!');
                            jQuery('#books').DataTable().destroy();
                            fetchData();
                        }

                    }
                });
            }
        }

        function returnBook(id)
        {
            var r = confirm("Are you sure?");
            if (r == true) {
                $.ajax({
                    "url": "/admin/book/return/"+id,
                    "method":"GET",
                    success: function(response){
                        if(response.code == 200)
                        {
                            alert('Book returned Successfully!!');
                            jQuery('#books').DataTable().destroy();
                            fetchData();
                        }

                    }
                });
            }
        }
    </script>
@endsection