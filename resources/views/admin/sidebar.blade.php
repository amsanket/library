<style type="text/css">
    .nav li{
        width: 200px;
    }
</style>
<div class="col-md-3">
    <div class="card card-default panel-flush">
        <div class="card-header">
            Sidebar
        </div>
        <div class="card-body">
            <ul class="nav" role="tablist">
                
                <li class="nav-items" role="presentation">
                    <a class="nav-links" href="{{ url('/admin/books') }}">
                        Books
                    </a>
                </li>
                <li role="presentation">
                    <a class="nav-links" href="{{ url('/admin/pending/issues') }}">
                        Pending Issues
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
