@extends('layouts.app')
@section('content')
<div class="container">
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
	<div class="row">
	    <div class="form-group">
	    	<a href="/books/add-new" class="btn btn-primary">Add New</a>
	    </div>
	    @if(Auth::user()->role !== 'member')
	    <div class="form-group">
	    	<a href="/admin/books" class="btn btn-primary">Admin</a>
	    </div>
	    @endif
	</div>
	<div class="pagination">
		{{ $books->links() }}
		
	</div>
	<div class="d-flex card-wrap flex-center row">
		@foreach($books as $book)
		<div class="book text-center">
			<div class="book-body">
				<a href="/books/details/{{$book->id}}">
					<i class="fa fa-book"></i>
					<span class="book-name"> {{$book->name}} </span><br>
					<span class="by-text">By</span><br>
					<span class="author-name"> {{$book->author}} </span>
					
				</a>
			</div>
		</div>
		@endforeach
	</div>
	<div class="pagination">
		{{ $books->links() }}
		
	</div>

</div>
@endsection