@extends('layouts.app')
@section('content')
<div class="container">
	@foreach (['danger', 'warning', 'success', 'info'] as $key)
 @if(Session::has($key))
     <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
 @endif
@endforeach
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<form method="post" action="/books/store">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="book_name" value="" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Author Name</label>
					<input type="text" class="form-control" name="author_name" value="" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Publisher Name</label>
					<input type="text" class="form-control" name="publisher" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>ISBN</label>
					<input type="text" class="form-control" name="isbn" value="" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Available Quantity</label>
					<input type="text" class="form-control" name="avl_quantity" value="" {{ Auth::user()->role == 'member' ? 'disabled' : '' }} >
				</div>
				{{csrf_field()}}
				
				<input type="submit" class="btn btn-primary" value="Create" name="">
			</form>
		</div>
	</div>
</div>
@endsection