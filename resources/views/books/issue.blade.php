@extends('layouts.app')
@section('content')
<div class="container">
	@foreach (['danger', 'warning', 'success', 'info'] as $key)
 @if(Session::has($key))
     <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
 @endif
@endforeach
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<form method="post" action="/books/update">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="book_name" value="{{$book->name}}" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Author Name</label>
					<input type="text" class="form-control" name="author_name" value="{{$book->author}}" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Publisher Name</label>
					<input type="text" class="form-control" name="publisher" {{ Auth::user()->role == 'member' ? 'disabled' : '' }} value="{{$book->publisher}}">
				</div>
				<div class="form-group">
					<label>ISBN</label>
					<input type="text" class="form-control" name="isbn" value="{{$book->isbn}}" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<div class="form-group">
					<label>Available Quantity</label>
					<input type="text" class="form-control" name="avl_quantity" value="{{$book->quantity}}" {{ Auth::user()->role == 'member' ? 'disabled' : '' }} >
				</div>
				<div class="form-group">
					<label>Outward Quantity</label>
					<input type="text" class="form-control" name="outward_quantity" value="{{$book->outward_quantity}}" {{ Auth::user()->role == 'member' ? 'disabled' : '' }}>
				</div>
				<input type="hidden" name="book_id" value="{{$book->id}}">
				<div class="form-group">
					<label>Total Quantity:</label>
					{{$book->quantity + $book->outward_quantity}}
				</div>
				{{csrf_field()}}
				@if(Auth::user()->role === 'member')
				<input type="submit" class="btn btn-primary" value="Apply" name="">
				@else
				<input type="submit" class="btn btn-primary" value="Update" name="">
				@endif
			</form>
		</div>
	</div>
</div>
@endsection