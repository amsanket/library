<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newUser = User::create([
									'name' => 'sanket',
									'email' => 'amsanket22@gmail.com',
									'password' => bcrypt('sanket123'),
									'role' => 'admin'
								]);

        $newUser = User::create([
									'name' => 'john',
									'email' => 'john@gmail.com',
									'password' => bcrypt('john123'),
									'role' => 'member'
								]);
    }
}
