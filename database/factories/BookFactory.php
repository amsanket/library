<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'publisher' => $faker->name,
        'quantity' => $faker->randomDigit,
        'author' => $faker->name,
        'isbn' => $faker->isbn10,
    ];
});
