# library

## About Project

I have developed small demonstration of library management system. there are two roles admin or manager and member. 

Members Duties - 
- He can see books in the gallery view.
- apply for that book if it is available.
- once admin approves it, he can take that book.

Admin - 

- Can Edit/update the book in the list view. see list of books at backend(/admin routes). 

- can see all the transactions.

- can approve the issue of the book.

- can add the return entry when the book is returned by the member.

book quantity gets updated(increment/decrement) as per the transaction

## Technical Notes

Custom middleware to check role. so that member is restricted to use admin area.

Custom Datatables(without using Yajra Package)

## Installation Process
```
git clone https://amsanket@bitbucket.org/amsanket/library.git
```
give appropriate permissions to the directory if required
```
cd library
composer install
```
create database in your mysql
update env
```
php artisan key:generate
php artisan config:cache
php artisan migrate --seed
```

## In Progress -

Developement of the package
#### Suggestions are welcome
