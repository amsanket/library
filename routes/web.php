<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {

Route::get('/books','BookController@index');

Route::get('/books/add-new','BookController@create');

Route::post('/books/store','BookController@store');

Route::get('/books/details/{id}','BookController@details');

Route::post('/books/update','BookController@issueBook');
});

Route::group(['middleware' => ['auth','role']],function() {


Route::get('/admin/books','AdminController@showBookList');

Route::get('/admin/pending/issues','AdminController@showPendingIssues');

Route::get('/admin/book-data','AdminController@bookData');

Route::get('/admin/pending/data','AdminController@pendingData');

Route::get('/admin/issue/approve/{id}','AdminController@approveIssue');

Route::get('/admin/book/return/{id}','AdminController@returnBook');

});
