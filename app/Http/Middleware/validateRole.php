<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class validateRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user()->role);
        if (!in_array(Auth::user()->role,['admin','manager'])) {
            return redirect('/books');
        }
        return $next($request);
    }
}
