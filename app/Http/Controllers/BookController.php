<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Transaction;
use Auth;
use Carbon\Carbon;
use DB;
use Redirect;

class BookController extends Controller
{
    public function create()
    {
        return view('books.create');
    }

    public function index()
    {
    	$books = Book::orderBy('id','desc')->paginate(10);
        
    	return view('books.index',compact('books'));
    }

    public function store(Request $request) 
    {
        $data = $request->all();
        $this->validate($request,['avl_quantity' => 'required|integer','book_name' => 'required','author_name'=> 'required','isbn' => 'required|unique:books']);
        $book = new Book;
        $book->name = $data['book_name'];
        $book->author = $data['author_name'];
        $book->publisher = $data['publisher'];
        $book->isbn = $data['isbn'];
        $book->quantity = $data['avl_quantity'];
        $book->save();
        return Redirect::to('/books')->with('success','New book added successfully!');
    }

    public function details($id)
    {
    	$book = Book::find($id);
        $book->outward_quantity = $book->transactions->where('returned_on',null)->count();
    	if(Auth::user())
    	{
	    	return view('books.issue',compact('book'));
    	}else {
    		return redirect('login');
    	}
    }

    public function issueBook(Request $request)
    {
    	$data = $request->all();
    	if(Auth::user()->role == 'member')
    	{
            $book = Book::find($data['book_id']);
            if($book->quantity < 1)
            {
               return redirect()->back()->with('danger','This book is not available'); 
            }
            try{

            DB::beginTransaction();
            $transaction = new Transaction;
            $transaction->book_id = $data['book_id'];
            $transaction->user_id = Auth::user()->id;
            $transaction->issued_on = Carbon::now();
            $transaction->save(); 
            $book = Book::find($data['book_id']);
            $book->decrement('quantity', 1);
            DB::commit();
            return Redirect::to('books')->with('success', 'You have successfully submitted the request, Admin will take the action');
            }
            catch (\Exception $ex) {
                DB::rollback();

                return Redirect::to('/book/details/'.$data['book_id'])->withErrors($ex->getMessage());
            }
    	}else {
            $book = Book::find($data['book_id']);
            $book->name = $data['book_name'];
            $book->author = $data['author_name'];
            $book->publisher = $data['publisher'];
            $book->isbn = $data['isbn'];
            $book->quantity = $data['avl_quantity'];
            $book->save();
            return redirect()->back()->with('success','Book has been successfully updated!'); 
    	}
    }

    
}
