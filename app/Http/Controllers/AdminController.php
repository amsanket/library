<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Book;
use DB;
use Carbon\Carbon;


class AdminController extends Controller
{
    public function bookData(Request $request)
    {
        $data = $request->all();

        $orderBy = 'books.id';
        $dir = 'desc';
        $limit = isset($data['length']) ? $data['length'] : 10;
        $offset = isset($data['start']) ? $data['start'] : 0;

        if (isset($data['book'])){
            $orderBy = $data['columns'][$data['book'][0]['column']]['data'];
            $dir = $data['book'][0]['dir'];
        }

        $books = Book::select('name','author','isbn','quantity');
        if (isset($data['search']['value']) && $data['search']['value'] != '') {
            $search = $data['search']['value'];
            $searchFields = array('books.name','books.author','books.isbn','books.quantity');
            $books->where(function($query) use ($search, $searchFields) {
                foreach($searchFields as $column) {
                    $query->orWhere($column,'like',"%" . $search . "%");
                }
            });
        }

        $totalRecords = $books->count();
        $booksData = $books->orderBy($orderBy,$dir)
            ->limit($limit)->offset($offset)
            ->get()->toArray();

        return response()->json(array('iTotalRecords' => $totalRecords, 'iTotalDisplayRecords' => $totalRecords, 'aaData' => $booksData, 'data' => $data));
    }

    public function pendingData(Request $request)
    {
        $data = $request->all();

        $orderBy = 'books.id';
        $dir = 'desc';
        $limit = isset($data['length']) ? $data['length'] : 10;
        $offset = isset($data['start']) ? $data['start'] : 0;

        if (isset($data['book'])){
            $orderBy = $data['columns'][$data['book'][0]['column']]['data'];
            $dir = $data['book'][0]['dir'];
        }

        $transactions = Transaction::join('users', 'users.id', '=', 'transactions.user_id')
                    ->join('books','books.id','=','transactions.book_id')
                    ->select('users.name as user', 'books.name','books.isbn','books.quantity', 'transactions.status', 'transactions.id','transactions.issued_on','transactions.id');
        if (isset($data['search']['value']) && $data['search']['value'] != '') {
            $search = $data['search']['value'];
            $searchFields = array('books.name','user.name','books.isbn','books.quantity');
            $transactions->where(function($query) use ($search, $searchFields) {
                foreach($searchFields as $column) {
                    $query->orWhere($column,'like',"%" . $search . "%");
                }
            });
        }

        $totalRecords = $transactions->count();
        $transactionData = $transactions->orderBy($orderBy,$dir)
            ->limit($limit)->offset($offset)
            ->get()->toArray();

        return response()->json(array('iTotalRecords' => $totalRecords, 'iTotalDisplayRecords' => $totalRecords, 'aaData' => $transactionData, 'data' => $data));

    }

    public function approveIssue($id) 
    {
        $transaction = Transaction::find($id);
        $transaction->status = 1;
        $transaction->save();
        return response()->json([
            'code' => 200
        ]);
    }

    public function returnBook($id)
    {
    	$transaction = Transaction::find($id);
    	$transaction->status = 2;
    	$transaction->returned_on = Carbon::now();
    	$transaction->save();
    	$book = Book::find($transaction->book_id);
    	$book->increment('quantity',1);
    	return response()->json([
            'code' => 200
        ]);

    }

    public function showBookList()
    {
        return view ('admin.index');
    }

    public function showPendingIssues()
    {
        return view ('admin.pending');
    }
}
